Author: Yuhe Yuan

Environment Prerequisites: Browser that supports css, javascript and html, Bash Environment

Instructions on how to build and run the project
1. Clone the repository into your local storage by using the git command below:
git clone https://Alpacaaaaa@bitbucket.org/Alpacaaaaa/dragon-ball.git
2. open the folder created and double click to open index.html
3. You now should have the dragon ball page opened on your browser which will display all character information

You can also visit the live site for this project on https://dragon-ball-8a813.firebaseapp.com/
or you may create following the steps below.

If you wish to build the project live on FireBase, please follow the instruction below:
1. go to firebase.google.com -> Console
2. Create a new project
3. Install NodeJs if you do not have it
4. open command prompt CMD and run "npm install -g firebase-tools" to install firebase on the local machine
5. run "firebase login" command to log in
6. direct command prompt directory to the folder you have all the files of this git project
7. create a folder named public and move all files of this git project into the public folder
8. run "firebase init"  to initialize firebase and select the project you created
9. Answer Yes to "Are you ready to proceed"
10. use space to select Hosting Option when asked for Firebase CLI features
11. enter "public" as the folder name when asked for public directory
12. answer no when asked to configure as a single-page app
13. answer no when asked that the public/index.html already exist, Overwrite?
14. run command "firebase deploy"
15. go to the website that is displayed on the screen.


Explaination of choice of License: The MIT License(https://opensource.org/licenses/MIT) is permissive as the modified versions does not have to be issued under the same licensence, files does not have to contain all information about changes, copyrights and patents and it does not prohibit the use of copyright holder's name for promotion. The persmissiveness would allow me to combine codes with other open source libraries with only few restrictions. The license is also simple unlike many other licenses with long and complex text.